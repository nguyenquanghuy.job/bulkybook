﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BulkyBook.DataAccess.Repository;
using BulkyBook.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BulkyBookWeb.Controllers
{
    [Area("Admin")]
    public class CoverTypeController : Controller
    {
        // GET: /<controller>/
        private readonly IUnitOfWork _unitOfWork;

        public CoverTypeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IActionResult Index()
        {
            var coverTypes = _unitOfWork.CoverTypeRepository.GetAll().OrderByDescending(o => o.Id);

            return View(coverTypes);
        }

        public IActionResult Create()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CoverType coverType)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.CoverTypeRepository.Add(coverType);
                _unitOfWork.Save();

                TempData["success"] = $"Create Cover Type successfully!!!";

                return RedirectToAction("Index");
            }
            return View(coverType);
        }

        public IActionResult Update(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var coverType = _unitOfWork.CoverTypeRepository.GetFirstOrDefault(o => o.Id == id);

            if (coverType == null)
            {
                return NotFound();
            }

            return View(coverType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(CoverType coverType)
        {
            if (ModelState.IsValid)
            {
                _unitOfWork.CoverTypeRepository.Update(coverType);
                _unitOfWork.Save();

                TempData["success"] = $"Update Cover Type successfully!!!";

                return RedirectToAction("Index");
            }
            return View(coverType);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null || id == 0)
            {
                return NotFound();
            }
            var coverType = _unitOfWork.CoverTypeRepository.GetFirstOrDefault(o => o.Id == id);

            if (coverType == null)
            {
                return NotFound();
            }

            return View(coverType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(int? id)
        {
            var coverType = _unitOfWork.CoverTypeRepository.GetFirstOrDefault(o => o.Id == id);
            if (coverType is null)
            {
                return NotFound();
            }

            _unitOfWork.CoverTypeRepository.Remove(coverType);
            _unitOfWork.Save();

            TempData["success"] = $"Cover Type {coverType.Name} has been deleted!!!";

            return RedirectToAction("Index");

        }
    }
}


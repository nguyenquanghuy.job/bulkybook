﻿using System;
using BulkyBook.Models;

namespace BulkyBook.DataAccess.Repository
{
    public interface IProductRepository : IRepository<Product>
    {
        void Update(Product product);
    }
}


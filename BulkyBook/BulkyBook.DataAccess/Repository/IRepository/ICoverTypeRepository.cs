﻿using System;
using BulkyBook.Models;

namespace BulkyBook.DataAccess.Repository
{
    public interface ICoverTypeRepository : IRepository<CoverType>
    {
        void Update(CoverType coverType);
    }
}


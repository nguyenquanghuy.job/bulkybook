﻿using System;
using System.Linq.Expressions;

namespace BulkyBook.DataAccess.Repository
{
    public interface IRepository<T> where T: class
    {
        T GetFirstOrDefault(Expression<Func<T,bool>> filter,string? includeProperties = null);
        IEnumerable<T> GetAll(string? includeProperties = null);
        void Add(T obj);
        void Remove(T obj);
        void RemoveRange(IEnumerable<T> entities);
    }
}


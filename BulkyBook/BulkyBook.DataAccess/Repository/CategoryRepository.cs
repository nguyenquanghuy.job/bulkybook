﻿using System;
using System.Linq.Expressions;
using BulkyBook.Models;

namespace BulkyBook.DataAccess.Repository
{
    public class CategoryRepository : Repository<Category>,ICategoryRepository
    {
        private AppDbContext _context;
        public CategoryRepository(AppDbContext context) : base(context)
        {
            _context = context;
        }
        public void Update(Category category)
        {
            _context.Categories.Update(category);
        }

    }
}


﻿using System;
using BulkyBook.Models;
using Microsoft.EntityFrameworkCore;

namespace BulkyBook.DataAccess
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().Property(c => c.CreatedDate).ValueGeneratedOnAdd();
            modelBuilder.Entity<Category>().Property(c => c.UpdatedDate).ValueGeneratedOnAddOrUpdate();
            modelBuilder.Entity<Category>().Property(c => c.IsDeleted).HasDefaultValue(false);


            modelBuilder.Entity<Category>().HasData(
                new Category
                {
                    Id = 1,
                    Name = "test1",
                    DisplayOrder = 1
                }, new Category
                {
                    Id = 2,
                    Name = "test2",
                    DisplayOrder = 2
                }, new Category
                {
                    Id = 3,
                    Name = "test3",
                    DisplayOrder = 3
                });
        }

        //public override int SaveChanges()
        //{
        //    foreach(var changedEntity in ChangeTracker.Entries())
        //    {
        //        if(changedEntity.Entity is Category entity)
        //        {
        //            switch (changedEntity.State)
        //            {
        //                case EntityState.Modified:
        //                    Entry(entity).Property(x=>x.CreatedDate).IsModified = false;
        //                    break;
        //            }
        //        }
        //    }
        //    return base.SaveChanges();
        //}

        public DbSet<Category> Categories { get; set; }
        public DbSet<CoverType> CoverTypes { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}

